# Duke3D Palette

[![NPM version][npm-version-image]][npm-url]
[![Build Status][build-status-image]][build-status-url]
[![Coverage Status][coverage-image]][coverage-url]
[![Known Vulnerabilities][vulnerabilities-image]][vulnerabilities-url]
[![Downloads][npm-downloads-image]][npm-url]

A module for reading Duke Nukem 3D palette files.

## Usage

```javascript
var Palette = require("duke3d-palette");

TODO
```

## Installation

To install this module:
```bash
npm install duke3d-palette
```

[npm-url]: https://www.npmjs.com/package/duke3d-palette
[npm-version-image]: https://img.shields.io/npm/v/duke3d-palette.svg
[npm-downloads-image]: http://img.shields.io/npm/dm/duke3d-palette.svg

[build-status-url]: https://travis-ci.org/nitro404/duke3d-palette
[build-status-image]: https://travis-ci.org/nitro404/duke3d-palette.svg?branch=master

[coverage-url]: https://coveralls.io/github/nitro404/duke3d-palette?branch=master
[coverage-image]: https://coveralls.io/repos/github/nitro404/duke3d-palette/badge.svg?branch=master

[vulnerabilities-url]: https://snyk.io/test/github/nitro404/duke3d-palette?targetFile=package.json
[vulnerabilities-image]: https://snyk.io/test/github/nitro404/duke3d-palette/badge.svg?targetFile=package.json
